const express = require('express');
const router = express.Router();

const controller = require('../Controllers/TrainingController');

router.get('/', controller.getAllTrainings);
router.post('/', controller.addTraining);

router.get('/types', controller.getAllTrainingTypes);

router.get('/:id', controller.getTrainingById);
router.patch('/:id', controller.updateTrainingById);
router.delete('/:id', controller.removeTrainingById);

router.get('/:id/trainees', controller.getTraineesById);

module.exports = router;