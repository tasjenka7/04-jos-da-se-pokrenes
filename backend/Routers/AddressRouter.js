const express = require('express');
const router = express.Router();

const controller = require('../Controllers/AddressController');

router.get('/', controller.getAllAddresses);
router.post('/', controller.addAddress);

router.get('/:id', controller.getAddressById);
router.put('/:id', controller.updateAddressById);
router.delete('/:id', controller.removeAddressById);

module.exports = router;