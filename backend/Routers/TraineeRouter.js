const express = require('express');
const router = express.Router();

const controller = require('../Controllers/TraineeController');

router.get('/', controller.getAllTrainees);
router.post('/', controller.addTrainee);

router.get('/:id', controller.getTraineeById);
router.patch('/:id', controller.updateTraineeById);
router.delete('/:id', controller.removeTraineeById);

router.get('/:id/trainings', controller.getAllTrainingsFromTraineeById);

router.delete('/:traineeId/:trainingId', controller.unsubscribeFromTrainingByTrainingId);

module.exports = router;