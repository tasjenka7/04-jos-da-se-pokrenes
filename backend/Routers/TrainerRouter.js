const express = require('express');
const router = express.Router();

const controller = require('../Controllers/TrainerController');

router.get('/', controller.getAllTrainers);
router.post('/', controller.addTrainer)

router.get('/:id', controller.getTrainerById);
router.put('/:id', controller.updateTrainerById);;
router.delete('/:id', controller.removeTrainerById);

module.exports = router;