const messageParser = require("./MessageParser");

const jwt = require('jsonwebtoken');
const secretKey = require('../config/secretkey.json');

const ChatRoom = require("../../Models/ChatRoomModel");
const Message = require("../../Models/MessageModel");
const User = require("../../Models/UserModel");
const { create } = require("../../Models/UserModel");
const { createChatRoom } = require("../../Controllers/ChatController");

const getJwtFromQuery = (url) => {
    const index = url.indexOf("=");

    const jwtString = url.substring(index+1, url.length);

    return jwtString;
};

const onCreatedRoomReceived = async function (createdRoomData) {
    const name = createdRoomData.value.chatRoom.name;
    const userId = createdRoomData.value.user.id;
    const users = createdRoomData.value.chatRoom.users.map((e) => e.id);
    const time = Date.now();

    const createdChatRoom = await ChatRoom.createChatRoom(name, userId, users);

    const chatRoomId = createdChatRoom._id;

    const user = await User.findById(userId);
    const chatRoom = await ChatRoom.getChatRoomById(chatRoomId);
    
    const message = { text: user.username + " has created the chat.", time };
    const eventType = "createdRoom";
    
    await ChatRoom.addMessage({
        message: message.text,
        time,
        chatRoom: chatRoomId,
    });

    chatRoom.users
        .filter((e) => e._id != userId)
        .forEach(async (e) => {
            message.text = e.username + " has joined the chat.";

            await ChatRoom.addMessage({
                message: message.text,
                time,
                chatRoom: chatRoomId,
            });
        });

    return {
        eventType,
        value: {
            user: { id: user.id, username: user.username },
            chatRoom: {
                id: chatRoom._id,
                messages: message.text,
                name: chatRoom.name,
                users: chatRoom.users.map((e) => ({
                    id: e._id,
                    username: e.username,
                    iconUrl: "todo",
                })),
            },
            message,
        },
    };
};

const onAddedUserReceived = async function (addedUserData) {
    const chatRoomId = addedUserData.value.chatRoom.id;
    const userId = addedUserData.value.user.id;
    const time = Date.now();

    await ChatRoom.addUsers(chatRoomId, [userId]);

    const user = await User.getById(userId);
    const chatRoomHeader = await ChatRoom.getChatRoomHeader(chatRoomId);
    const message = { text: user.username + " has joined the chat.", time };
    const eventType = "addedUser";
    
    await ChatRoom.addMessage({
        message: message.text,
        time,
        chatRoom: chatRoomId,
    });
    return {
        eventType,
        value: {
            user: { 
                    id: user.id,
                    username: user.username,
                    firstname : user.firstName,
                    lastname: user.lastName
                 },
            chatRoom: {
                id: chatRoomHeader._id,
                messages: chatRoomHeader.messages[0],
                name: chatRoomHeader.name,
            },
            message,
        },
    };
};

const onRemovedUsedReceived = async function (removedUserDate) {
    const chatRoomId = removedUserDate.value.chatRoom.id;
    const userId = removedUserDate.value.user.id;
    const time = Date.now();

    await ChatRoom.removeUsers(chatRoomId, [userId]);

    const user = await User.getById(userId);
    const chatRoomHeader = await ChatRoom.getChatRoomHeader(chatRoomId);
    const message = { text: user.username + " has left the chat.", time };
    const eventType = "removedUser";

    await ChatRoom.addMessage({
        message: message.text,
        time,
        chatRoom: chatRoomId,
    });

    return {
        eventType,
        value: {
            user: { id: user.id, username: user.username },
            chatRoom: {
                id: chatRoomHeader._id,
                messages: chatRoomHeader.messages,
                name: chatRoomHeader.name,
            },
            message,
        },
    };
};

const onMessageReceived = async function (messageData) {
    const chatRoomId = messageData.value.chatRoom.id;
    const userId = messageData.value.user.id;
    const messageText = messageData.value.message.text;
    const time = Date.now();

    await ChatRoom.addMessage({
        message: messageText,
        user: userId,
        time,
        chatRoom: chatRoomId,
    });

    const user = await User.getById(userId);
    const chatRoomHeader = await ChatRoom.getChatRoomHeader(chatRoomId);
    const message = { text: messageText, time };
    const eventType = "message";

    return {
        eventType,
        value: {
            user: { id: user.id, username: user.username },
            chatRoom: {
                id: chatRoomHeader._id,
                messages: chatRoomHeader.messages,
                name: chatRoomHeader.name,
            },
            message,
        },
    };
};

const onChatEvent = function (server) {
    return async function (data) {
        try {
            console.log("Message received.");

            var messageDataJson = messageParser.getJson(data.utf8Data);
            
            var response = null;

           if (messageDataJson.eventType !== "createdRoom") {
            var chatRoomId = messageDataJson.value.chatRoom.id;
            
            var users = await ChatRoom.getUsers(chatRoomId);
            var userIds = users.map(e => e.id);
           }

            if (messageDataJson.eventType === "addedUser") {
                response = await onAddedUserReceived(messageDataJson);
                userIds.push(response.value.user.id);
                
            } else if (messageDataJson.eventType === "createdRoom") {
                response = await onCreatedRoomReceived(messageDataJson);

                chatRoomId = response.value.chatRoom.id;

                users = await ChatRoom.getUsers(chatRoomId);
                userIds = users.map(e => e.id);
            } else if (messageDataJson.eventType === "removedUser") {
                response = await onRemovedUsedReceived(messageDataJson);

            } else if (messageDataJson.eventType === "message") {
                response = await onMessageReceived(messageDataJson);
            }
            
            connections.forEach(e => {
                var connection = e.connection;
                var httpRequest = e.httpRequest;
                
                var user = httpRequest.user;

                // Send only to the relevant users (connections)
                if(userIds.some(e => e === user.id)){
                    connection.sendUTF(JSON.stringify(response));
                }
            });

        } catch (err) {
            console.log(err);
            // Check how tf am i going to handle errors here
        }
    };
};

var connections = [];

const onConnect = (connection) => {
    console.log("WebSocket connection established.");
};

const onClosed = (connection, reason, desc) => {
    console.log("WebSocket connection closed.");

    //TODO: 
    // const index = connections.map(e => e.httpRequest.url).indexOf(url, 0);

    // if (index > -1) {
    //  connections.splice(index, 1);
    // }
};

const onRequest = async function (request) {
    const connection = request.accept();

    const httpRequest = request.httpRequest;
    
    const token = getJwtFromQuery(httpRequest.url);
    
    const key = secretKey.secretkey;

    // Handle unauthorized
    const jwtPayload = jwt.verify(token, key);
    const user = await User.getById(jwtPayload.id);
    
    httpRequest.user = user;

    connections.push({ connection, httpRequest });

    console.log(new Date() + " Connection accepted.");

    connection.on("message", onChatEvent(this)); // Saves a message and broadcasts it to all listeners
};


module.exports = {
    onConnect: onConnect,
    onClosed: onClosed,
    onRequest: onRequest,
};
