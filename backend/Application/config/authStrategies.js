const passportConf = require('../config/passport');
const secretKey = require('./secretkey.json')

const LocalStrategy = require('passport-local').Strategy;

const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const User = require('../../Models/UserModel');

const localLoginOptions = {
    session : false
};

const localLoginStrategy = new LocalStrategy(localLoginOptions,
    async(username, password, done) => {
        try {
            const user = await User.getByUsername(username);
         
            if (!user) {
                return done(null, false, { message: 'Incorrect username' });
            }

            //TODO: Move this to a function
            if(!(user.password === password)){
                return done(null, false, { message: 'Incorrect password.' });
            }

            return done(null, user);

        } catch (err) {
            return done(err);
        }
    },
);

const jwtOptions = {
    //TODO: NK: Maybe use Bearer token?
    // Requests should have the following header:
    //          Authorization: JWT jwt_token_string
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('JWT'),
    secretOrKey: secretKey.secretkey
};

const jwtStrategy = new JwtStrategy(jwtOptions,
    async(jwt_payload, done) => {
        try{
            const userId = jwt_payload.id;
            // For now, the payload will carry only an user's id
            
            const user = await User.findById(userId);
    
            if(user){
                done(null, user);
            }
            else{
                done(null, false);
            }
        }
        catch(err){
            return done(err);
        }
    }
);


module.exports.localLogin = localLoginStrategy;
module.exports.jwt = jwtStrategy;