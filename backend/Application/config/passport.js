const passport = require('passport');
const User = require('../../Models/UserModel');

passport.serializeUser((user, done) => {
    //TODO: User.kind so we can differ between the trainers and trainees
    done(null, {
        id : user._id,
        kind: user.kind
    });
});

passport.deserializeUser(async(userSessionData, done) => {
    try {
        
        const retrievedUser = await User.getById(userSessionData.id);
        
        return retrievedUser ? done(null, retrievedUser) : done(new Error('User not found'));
    } catch (err) {
        done(err);
    }
});

