const mongoose = require('mongoose');

const statusScheme = new mongoose.Schema({
    _id:{
        type: mongoose.Schema.Types.ObjectId,
        auto: true
    },
    date:{
        type: Date,
        required: true,
    },
    content:{
        type: String,
        required: true,
    }
});

statusScheme.statics.getAll = async function () {
    return await this.find({}, {__v: 0}).exec();
}

statusScheme.statics.getById = async function (id) {
    const status = await this.find({ _id : id }, { __v: 0 }).exec();

    if (!status) {
        return Promise.reject();
    } else {
        return status;
    }
};

statusScheme.statics.add = async function (status) {
    const addedStatus = new this(status);
    await addedStatus.save();
    
    const retrivedStatus = await this.getById(addedStatus._id);

    if (!retrivedStatus) {
        return Promise.reject();
    } else {
        return retrivedStatus;
    }
};

statusScheme.statics.removeById = async function (id) {
    const status = await this.findById(id);

    if(status) {
        await this.deleteOne({ _id: id }).exec();
    } else {
        return Promise.reject();
    }
};

const statusModel = mongoose.model("Status", statusScheme);

module.exports = statusModel;