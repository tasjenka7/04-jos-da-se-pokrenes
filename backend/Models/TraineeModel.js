const mongoose = require('mongoose');
const User = require('./UserModel');
const Training = require('./TrainingModel')

const traineeScheme = new mongoose.Schema({
    trainings: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Training',
        defaulte: [],
        required: true
    }]
});

traineeScheme.statics.getAll = async function(){
    return await this
        .find({}, {__v: 0})
        .exec();
};

traineeScheme.statics.getById = async function(id){
    const trainee = await this
        .findById(id, { __v: 0})
        .exec();

        if(!trainee){
            return Promise.reject();
        }
        else{
            return trainee;
        }
};

traineeScheme.statics.checkAndUpdate = async function(trainee){    
    const id = await this
        .findOne({_id : trainee._id})
        .select('_id')
        .lean()
        .exec();

    if(!id){
        return Promise.reject();
    }
    else{
        await this
            .updateOne({_id : trainee._id}, { $set: trainee})
            .exec();

        return await this.getById(id);
    }
};

traineeScheme.statics.add = async function(trainee){    
    const addedTrainee = new this(trainee);
    await addedTrainee.save();
    
    const retrivedTrainee = await this.getById(addedTrainee._id);
    
    if(!retrivedTrainee){
        return Promise.reject();
    }
    else{
        return retrivedTrainee;
    }
};

traineeScheme.statics.removeById = async function(id){
    const trainee = await this.getById(id);

    if(trainee) {
            await this.deleteOne({ _id: id}).exec();
    } else {
        Promise.reject();
    }    
};

traineeScheme.statics.removeTraining = async function(trainingId) {
    await this.updateMany({}, {$pull: {trainings: trainingId}});
};

traineeScheme.statics.unsubscribeFromTrainingByTrainingId = async function(traineeId, trainingId) {
    const trainee = await this.getById(traineeId);
    //const training = await Training.getById(trainingId);

    if(trainee){
        await this.update({_id: traineeId}, {$pull: {trainings: trainingId}}).exec();
        await Training.update({_id: trainingId}, {$pull: {traineesApplied: traineeId}}).exec();
    }
};

traineeScheme.statics.getAllTrainings = async function(id) {
    const trainee = await this.findById(id)
        .populate("trainings", {__v: 0})
        .exec();

    if(trainee) {
        return trainee.trainings;
    }
    else {
        Promise.reject();
    }
}

const traineeModel = User.discriminator('Trainee', traineeScheme);

module.exports = traineeModel;