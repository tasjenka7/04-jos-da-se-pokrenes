const mongoose = require('mongoose')

const AddressModel = require('./AddressModel');
const GymModel = require('./GymModel');

const TrainerModel = require('./TrainerModel');
const TrainingModel = require('./TrainingModel');

const TraineeModel = require('./TraineeModel');

const ChatRoom = require('./ChatRoomModel');
const Message = require('./MessageModel');
const StatusModel = require('./StatusModel');


module.exports.initMockData = async () =>{
  for(i =0 ; i < 10; i++){

    const address = new AddressModel({
      _id: mongoose.Types.ObjectId(),
      streetName: 'Street Name',
      streetNumber: 42,
      city: 'City',
      country: 'Country',
      zip: 11000
    });
    
    const gym = new GymModel({
      _id: mongoose.Types.ObjectId(),
      name: 'Gym',
      address: address,
      phone: 'phone number',
      startTime: Date.now(),
      endTime: Date.now()
    });
    
    const trainer = new TrainerModel({
      _id: mongoose.Types.ObjectId(),
      username: 'username' + i,
      password: 'password' + i,
      firstName: 'firstname ' + i,
      lastName: 'lastname ' + i,
      email: 'email@email.com',
      credits: 42,
      imgPath: 'path/path/path',
      birthday: Date.now(),
      sports: ['squats', 'moresquats', 'onlysquats'],
      ratings: [5, 5, 5, 5],
      trainings: []
    });

    const status = new StatusModel({
      _id: mongoose.Types.ObjectId(),
      date: Date.now(),
      content: "Hello everyone!"
    });

    trainer.statuses.push(status);
    
    const training = new TrainingModel({
      _id: mongoose.Types.ObjectId(),
      startTime: Date.now(),
      endTime: Date.now(),
      difficulty: 5,
      name: 'Ultimate weigth lifting',
      gym: gym,
      maxSpots: 42,
      currentSpots: 21,
      trainingType: 'Weight lifting',
      price: 42,
      trainer: trainer,
      traineesApplied: []
    });
    
    trainer.trainings.push(training);
    
    const trainee = new TraineeModel({
      _id: mongoose.Types.ObjectId(),
      username: 'username' + i,
      password: 'password' + i,
      firstName: 'firstname',
      lastName: 'lastname',
      email: 'email@email.com',
      credits: 42,
      imgPath: 'path/path/path',
      birthday: Date.now(),
    });
    
    trainee.followers.push(trainer);
    
    training.traineesApplied.push(trainee);

    trainee.trainings.push(training);
    
    await address.save();
    await gym.save();
    await trainer.save();
    
    await training.save();
    
    await trainee.save();

    await status.save();
  
    for(i = 0; i < 10; i++){
      
      const chatRoom = new ChatRoom({
        _id: mongoose.Types.ObjectId(),
        name: 'rooooooom ' + i,
        createdBy: trainer,
        users: [trainer, trainee],
        messages: []
      });
      
      const messages = [
        new Message({
        _id: mongoose.Types.ObjectId(),
        message: 'aaaaaa'  + i,
        user: trainer,
        time: Date.now(),
        chatRoom: chatRoom}),
        
        new Message({
        _id: mongoose.Types.ObjectId(),
        message: 'bbbb' + i,
        user: trainer,
        time: Date.now(),
        chatRoom: chatRoom}),
      
        new Message({
        _id: mongoose.Types.ObjectId(),
        message: 'ccccccccccccccccccc' + i,
        user: trainer,
        time: Date.now(),
        chatRoom: chatRoom}),
        new Message({
          _id: mongoose.Types.ObjectId(),
          message: 'qweqweqwe' + i,
          user: trainer,
          time: Date.now(),
          chatRoom: chatRoom}),
          
          new Message({
          _id: mongoose.Types.ObjectId(),
          message: 'okpoko' + i,
          user: trainer,
          time: Date.now(),
          chatRoom: chatRoom}),
        
          new Message({
          _id: mongoose.Types.ObjectId(),
          message: '123123' + i,
          user: trainer,
          time: Date.now(),
          chatRoom: chatRoom})  
      ];
      
      await chatRoom.save();
      
      await Promise.all(messages.map((e) => e.save()));
      
      chatRoom.messages = messages;
      await chatRoom.save();
    }
  }
};