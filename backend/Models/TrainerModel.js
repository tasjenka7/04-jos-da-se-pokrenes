const mongoose = require('mongoose');
const UserModel = require('./UserModel');

const trainerModelScheme = new mongoose.Schema({
    trainings: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Training',
        defaulte: [],
        required: true
    }],
    sports: [{
        type: String,
        defaulte: [],
        required: true
    }],
    ratings: [{
        type: Number,
        min: 1,
        max: 5
    }]
});

trainerModelScheme.statics.getAll = async function(){
    return await this
        .find({}, {__v: 0})
        .exec();
};

trainerModelScheme.statics.getById = async function(id){
    const trainer = await this
        .findById(id, { __v: 0})
        .exec();

        if(!trainer){
            return Promise.reject();
        }
        else{
            return trainer;
        }
};

trainerModelScheme.statics.checkAndUpdate = async function(trainer){    
    const id = await this
        .findOne({_id : trainer._id})
        .select('_id')
        .lean()
        .exec();

    if(!id){
        return Promise.reject();
    }
    else{
        await this
            .updateOne({_id : trainer._id}, { $set: trainer})
            .exec();

        return await this.getById(id);
    }
};

trainerModelScheme.statics.add = async function(trainer){    
    const addedTrainer = new this(trainer);

    await addedTrainer.save();
    
    const retrivedTrainer = await this.getById(addedTrainer._id);
    
    if(!retrivedTrainer){
        return Promise.reject();
    }
    else{
        return retrivedTrainer;
    }
};

trainerModelScheme.statics.removeById = async function(id){
    const trainer = await this.getById(id);

    if(trainer) {
        await this.deleteOne({ _id: id}).exec();
    } else {
        return Promise.reject();
    }    
};

const trainerModel = UserModel.discriminator('Trainer', trainerModelScheme);

module.exports = trainerModel;