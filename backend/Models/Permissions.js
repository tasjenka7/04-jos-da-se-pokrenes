// TODO: Make this into hasKeys that accepts an array of keys as second arg
const hasKey = (object, key) => {
    return key in object;
};

//TODO: Write permission check functions here and include them in UserPermissions

const allPermitted = (arguments) => { return true; };

// Expects an object that has user & profile props
const canUpdateTraining = (arguments) =>{ };

// Let's say only user can see his own profile 
const canViewProfile = (arguments) => {    
    if(hasKey(arguments, 'user') && hasKey(arguments, 'profile')){
        return arguments.user.id === arguments.profile.id;
    }
    else{
        return false;
    }
};


module.exports = {
    allPermitted: allPermitted,
    canViewProfile: canViewProfile,
    canUpdateTraining: canUpdateTraining
};