const notFoundHandler = require('../Application/Middlewares/NotFoundHandler');

const authHandlers = require('../Application/Middlewares/AuthHandler');

const User = require('../Models/UserModel');

const Trainee = require('../Models/TraineeModel');

const Trainer = require('../Models/TrainerModel');

const notFound = require('../Application/Middlewares/NotFoundHandler');

module.exports.loginUser = authHandlers.localLoginHandler;
//TODO: Delete 
module.exports.testAuthentication = authHandlers.jwtAuthenticationTest;

module.exports.registerUser = async (req, res, next) => {
    try {
        const user = await User.getByUsername(req.body.username);

        if (user) {
            return res.status(422).json({ message: "Prosledjeni username postoji! Izaberite drugi!" });
        }
        else {
            const body = {
                "username": req.body.username,
                "password": req.body.password,
                "firstName": req.body.firstName,
                "lastName": req.body.lastName,
                "email": req.body.email,
                "birthday": req.body.birthday,
                "gender" : req.body.gender
            };
            //TODO : OSPOSOBITI kreiranje: 
            switch (req.body.kind) {
                case "Trainee":
                    await Trainee.add(body);
                    break;
                case "Trainer":
                    await Trainer.add(new Trainer(body));
                    break;
                default:
                    break;
                }
                res.send({ message: "Uspesno ste se registrovali. Cestitamo!" });
        }
    } catch (err) {
        next(err);

    }
};

module.exports.getAllUsers = async (req, res, next) => {
    try {
        const users = await User.getAll();
        res.send(users);
    } catch (err) {
        next(err);
    }
};

module.exports.getUserById = [async (req, res, next) => {
    try {
        const user = await User.getById(req.params.id);
        res.send(user);
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.getUserByUsername = [async (req, res, next) => {
    try {
        const user = await User.getByUsername(req.query.username);
        res.send(user);
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.addStatusToUser = async (req, res, next) => {
    try {
        const userId = req.params.id;
        const status = req.body;
        const addedStatus = await User.addStatus(userId, status);
        res.send(addedStatus);
    } catch (err) {
        next(err);
    }
};

module.exports.getAllStatusesFromUser = async (req, res, next) => {
    try {
        const statuses = await User.getAllStatuses(req.params.id);
        res.send(statuses);
    } catch (err) {
        next(err);
    }
};

module.exports.getStatusFromUser = [async (req, res, next) => {
    try {
        const status = await User.getStatus(req.params.statusId);
        res.send(status);
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.removeStatusFromUser = [async (req, res, next) => {
    try {
        await User.removeStatus(req.params.userId, req.params.statusId);

        res.send({ _id: req.params.userId, status: req.parama.tatusId });
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.getAllFollowersByUserId = [async (req, res, next) => {
    try {
        const followers = await User.getAllFollowers(req.params.id);

        res.send(followers);
    } catch (err) {
        next(err);
    }
}];

module.exports.getAllSubscribersByUserId = [async (req, res, next) => {
    try {
        const subscribers = await User.getAllSubscribers(req.params.id);

        res.send(subscribers);
    } catch (err) {
        next(err);
    }
}];

module.exports.addSubscriberToUser = [async (req, res, next) => {
    try {
        const userId = req.params.userId;
        const subscriberId = req.params.subscriberId;

        await User.addSubscriber(userId, subscriberId);

        res.status(200).json({ message: 'Uspesno je dodat pretplatnik na korisnika!' });
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.deleteSubscriberFromUser = [async (req, res, next) => {
    try {
        const userId = req.params.userId;
        const subscriberId = req.params.subscriberId;

        await User.deleteSubscriber(userId, subscriberId);

        res.status(200).json({ message: 'Uspesno je obrisan pretplatnik sa korisnika!' });
    } catch (err) {
        next(err);
    }
},
notFound
];

module.exports.updateUserById = [ async(req, res, next) => {
    try {
        const user = await User.updateById(req.params.id, req.body);
        res.send(user);
    } catch (err) {
        next(err);
    }
},
notFound
];