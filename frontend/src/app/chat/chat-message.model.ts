import { ChatUser } from './chat-user.model';

export interface ChatMessage {
  chatRoomId: string;
  userId?: string;
  username?: string;
  message: string;
  time: Date;
}
