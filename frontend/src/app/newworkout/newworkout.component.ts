import { Component, OnInit, OnDestroy } from '@angular/core';
import { Workout } from '../models/workout.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Gym } from '../models/gym.model';
import { GymService } from '../services/gym.service';
import { Observable, Subscription } from 'rxjs';
import { WorkoutService } from '../services/workout.service';
import { timeStamp } from 'console';
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material/dialog';
import { NavbarService } from '../services/navbar.service';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
@Component({
  selector: 'app-newworkout',
  templateUrl: './newworkout.component.html',
  styleUrls: ['./newworkout.component.css']
})
export class NewworkoutComponent implements OnInit, OnDestroy {

  public workouts: Workout[] = [];
  public checkForm: FormGroup;
  public gyms: Gym[];
  public tTypes: string[];
  public gymNames: String[] = [];
  public gymId: string;
  public currentUser: User;
  private activeSub: Subscription[] = [];


  constructor(private formBuilder: FormBuilder,
    public gymService: GymService,
    private workoutService: WorkoutService,
    public navBar: NavbarService,
    private router: Router,
    private userService: UserService) {

    this.checkForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      gym: ['', [Validators.required]],
      startTime: ['', [Validators.required]],
      endTime: ['', [Validators.required]],
      maxSpots: [10, [Validators.required, Validators.min(1)]],
      difficulty: [1, [Validators.required]],
      trainingType: ['', [Validators.required]],
      description: ['', [Validators.required, Validators.minLength(10)]],
      price: [250, [Validators.required, Validators.min(0)]]
    });

    this.currentUser = this.userService.getCurrentUser();

  }

  ngOnInit(): void {
    const sub = this.gymService.getGymNames()
      .subscribe(res => {
        this.gymNames = res;
      });
    this.activeSub.push(sub);
    const sub2 = this.gymService.getGyms()
      .subscribe(res => {
        this.gyms = res;
      });
    this.activeSub.push(sub2);
    //window.alert(this.gyms);

    const sub3 = this.workoutService.getTrainingTypes()
      .subscribe(res => {
        this.tTypes = res;
      });
    this.activeSub.push(sub3);
    this.navBar.showNavbar();
  }

  ngOnDestroy() {
    this.activeSub.forEach((sub) => sub.unsubscribe());
  }

  public get name() {
    return this.checkForm.get('name');
  }

  public get gym() {
    return this.checkForm.get('gym');
  }

  public get startTime() {
    return this.checkForm.get('startTime');
  }

  public get endTime() {
    return this.checkForm.get('endTime');
  }

  public get maxSpots() {
    return this.checkForm.get('maxSpots');
  }

  public get difficulty() {
    return this.checkForm.get('difficulty');
  }

  public get trainingType() {
    return this.checkForm.get('trainingType');
  }

  public get description() {
    return this.checkForm.get('description');
  }

  public get price() {
    return this.checkForm.get('price');
  }


  //TODO: provera da li je datum pocetka manji od datuma kraja i da li je isti dan
  public addWorkout(data) {

    if (!this.checkForm.valid) {
      window.alert("Not valid! ");
      return;
    }
    if (this.startTime.value >= this.endTime.value) {
      window.alert("Ne može termin početka biti kasniji/isti od termina kraja!");
      return;
    }
    if (this.startTime.value < new Date() || this.endTime.value < new Date()) {
      window.alert("Ne mogu se birati termini pre trenutnog datuma");
      return;
    }

    this.gyms.forEach(g => {
      if (g.name === this.gym.value) {
        this.gymId = g._id;
        return;
      }
    });

    const body = {
      name: this.name.value,
      gym: this.gymId,
      startTime: this.startTime.value,
      endTime: this.endTime.value,
      maxSpots: this.maxSpots.value,
      currentSpots: 0,
      difficulty: this.difficulty.value,
      trainingType: this.trainingType.value,
      price: this.price.value,
      description: this.description.value,
      trainer: this.currentUser._id
    }
    const subPost = this.workoutService
      .addWorkout(body)
      .subscribe(
        (res) => console.log(res),
        (err) => console.log(err));
    this.activeSub.push(subPost);

    this.checkForm.reset();
    this.trainingAddedInform();
    this.router.navigate(['/timetable']);
  }

  public trainingAddedInform() {
    window.alert("Uspešno ste zakazali trening");
  }
}
