import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MainPageComponent } from './main-page/main-page.component';
import { NewworkoutComponent } from './newworkout/newworkout.component';
import { RegisterInfoComponent } from './register-info/register-info.component';
import { ChatMainComponent } from './chat/chat-main/chat-main.component';
import { ChatHeadersComponent } from './chat/chat-headers/chat-headers.component';
import { ChatWindowComponent } from './chat/chat-window/chat-window.component';
import { ChatUsersComponent } from './chat/chat-users/chat-users.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileUpdateComponent } from './profile-update/profile-update.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TimetableComponent } from './timetable/timetable.component';
import { AllWorkoutsComponent } from './all-workouts/all-workouts.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { SearchfilterPipe } from './chat/services/searchfilter.pipe';
import { CreateChatroomComponent } from './chat/create-chatroom/create-chatroom.component';
import { JwtInterceptor } from './auth/jwt.interceptor';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { WorkoutUpdateComponent } from './workout-update/workout-update.component';
import { GymInfoComponent } from './gym-info/gym-info.component';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { TraineeListComponent } from './trainee-list/trainee-list.component';
import { NgxJdenticonModule } from 'ngx-jdenticon';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    NavbarComponent,
    MainPageComponent,
    NewworkoutComponent,
    RegisterInfoComponent,
    ChatMainComponent,
    ChatHeadersComponent,
    ChatWindowComponent,
    ChatUsersComponent,
    ProfileComponent,
    ProfileUpdateComponent,
    TimetableComponent,
    AllWorkoutsComponent,
    SearchfilterPipe,
    CreateChatroomComponent,
    GymInfoComponent,
    WorkoutUpdateComponent,
    TraineeListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatTableModule,
    MatSortModule,
    BrowserModule,
    MatCardModule,
    MatPaginatorModule,
    MatButtonModule,
    NgxJdenticonModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
