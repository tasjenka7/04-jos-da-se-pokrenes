import { User } from './user.model';
import { Gym } from './gym.model';

export class Workout {
    constructor(public _id: string,
                public name: string,
                public gym: string, 
                public startTime: Date,
                public endTime: Date,
                public maxSpots: number,
                public currentSpots: number,
                public difficulty: number,
                public trainingType: string,
                public description: string,
                public price: number,
                public trainer: string, 
                public traineesApplied: [string],
                ) {
    }
}