import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { MainPageComponent } from './main-page/main-page.component';
import { NewworkoutComponent } from './newworkout/newworkout.component';
import { RegisterInfoComponent } from './register-info/register-info.component';
import { ChatMainComponent } from './chat/chat-main/chat-main.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileUpdateComponent } from './profile-update/profile-update.component';
import { TimetableComponent } from './timetable/timetable.component';
import { AllWorkoutsComponent } from './all-workouts/all-workouts.component';
import { WorkoutUpdateComponent } from './workout-update/workout-update.component';
import { GymInfoComponent } from './gym-info/gym-info.component';
import { TraineeListComponent } from './trainee-list/trainee-list.component';

//http://localhost:4200/ +path
const routes: Routes = [
  //localhost:4200/
  { path: '', component: HomeComponent },
  //localhost:4200/login
  { path: 'login', component: LoginComponent },
  //localhost:4200/register
  { path: 'register', component: RegisterComponent },
  //localhost:4200/:username for each user it will be different
  { path: 'main/:username', component: MainPageComponent },
  //localhost:4200/newworkout
  { path: 'newworkout', component: NewworkoutComponent },

  { path: 'succReg/:username', component: RegisterInfoComponent },
  { path: 'chat', component: ChatMainComponent },

  { path: 'profile/:username', component: ProfileComponent },

  { path: 'profileUpdate', component: ProfileUpdateComponent },
  { path: 'timetable', component: TimetableComponent },
  { path: 'wupdate/:wusername', component: WorkoutUpdateComponent},
  { path: 'gym/:gymId', component: GymInfoComponent},
  { path: 'traineeList/:wid', component: TraineeListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
