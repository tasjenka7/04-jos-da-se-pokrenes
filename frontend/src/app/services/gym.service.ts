import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Gym } from '../models/gym.model';

@Injectable({
  providedIn: 'root'
})
export class GymService {

  private gyms: Observable<Gym[]>;
  private gymNames: string[] = ['Teretana MATF', 'MATF2'];
  private readonly gymUrl = 'http://localhost:3000/gyms/';

  constructor(private http: HttpClient) {
    this.gyms = this.http.get<Gym[]>(this.gymUrl);
  }

  public getGyms() :Observable<Gym[]>{
    return this.gyms;
  }

  public getGymNames(): Observable<string[]>{
    return this.http.get<string[]>(this.gymUrl + 'names');
  }

  public getGymById(gid: string): Observable<Gym>{
    return this.http.get<Gym>(this.gymUrl+ gid);
  }
}
