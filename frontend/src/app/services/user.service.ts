import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { RegisterComponent } from '../register/register.component'
import { LoginComponent } from '../login/login.component';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Status } from '../models/status.model';
import { Workout } from '../models/workout.model';
import { retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly userLink = "http://localhost:3000/users/";
  private readonly traineeLink = "http://localhost:3000/trainees/";
  private readonly trainerLink = "http://localhost:3000/trainers/";
  private readonly workoutLink = "http://localhost:3000/trainings/";
  private allUsers: User[];

  private usersTrainer: User[];

  private usersTrainee: User[];
  private userSubTrainer: User[];
  public usernames: Array<string> = [];
  public currentUser: User;

  constructor(private http: HttpClient) {

    // http zahtev za dohvatanje user-a
    this.http.get<User[]>(this.userLink)
      .subscribe(res => {
        this.allUsers = res;
      });
  }



  public getUsers(): User[] {
    return this.allUsers;
  }


  public getUsernames(): Array<string> {
    this.allUsers.forEach(element => {
      this.usernames.push(element.username.toString());
    });
    return this.usernames;
  }

  public getUserById(id: string): Observable<User> {
    return this.http.get<User>(this.userLink + id);
  }

  public getUserByUsername(username: String): Observable<User> {
    return this.http.get<User>(this.userLink + "filterByUsername?username=" + username);
  }

  public addUser(data) {
    return this.http.post(this.userLink + "register", data);
  }

  public getStatuses(userId): Observable<Status[]> {
    return this.http.get<Status[]>(this.userLink + userId + "/statuses");
  }

  //dodavanje novog statusa = izmena kod user-a
  public addStatus(uid, status) {
    this.http.post(this.userLink + "stat/" + uid, status);
    return this.http.post<any>(this.userLink + "stat/" + uid, status);
  }

  public getUsersWorkouts(uid: String): Observable<Workout[]> {
    return this.http.get<Workout[]>(this.traineeLink + uid + "/trainings");
  }

  public getFollowers(uid: String): Observable<User[]> {
    return this.http.get<User[]>(this.userLink + uid + "/followers");
  }

  public getSubs(uid: String): Observable<User[]> {
    return this.http.get<User[]>(this.userLink + uid + "/subscribers");
  }

  public putCurrentUser(user: User) {
    this.currentUser = user;
    //    window.alert(this.currentUser);
  }
  public getCurrentUser(): User {
    return this.currentUser;
  }


  public updateUserById(id: String, data): Observable<User> {
    // console.log('Data: ', data);
    // console.log('putanja: ', this.userLink + id);
    return this.http.patch<User>(this.userLink + id, data).pipe();
  }


  public addWorkoutToUser(id: String, data): Observable<User> {
    return this.http.patch<User>(this.traineeLink + id, data);
  }

  public removeWorkout(id: String) {
    return this.http.delete<any>(this.traineeLink + this.currentUser._id + '/' + id);
  }

  public deleteSubFromUser(idUser: string, idSub: string) {
    return this.http.delete<any>(this.userLink + idUser + "/subscribe/" + idSub);
  }



  public getTrainers(): Observable<User[]> {
    return this.http.get<User[]>(this.trainerLink);
  }

  public getTrainee() :Observable<User[]>{
    return this.http.get<User[]>(this.traineeLink);
  }



  public login(username: string, password: string) {
    const body = { username, password };

    return this.http.post<any>(this.userLink + 'login', body);
  }

  public deeleteSubscribeFromUser(idUser: string, idSub: string) {
    return this.http.delete(this.userLink + idUser + "/subscribe/" + idSub);
  }

}
